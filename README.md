# Curso de CSS

## Temario

1. :heavy_plus_sign: Sintaxis básica
1. :heavy_plus_sign: Selectores básicos
1. :heavy_plus_sign: Selector de hijos directos y descendientes
1. :heavy_plus_sign: Selector de hermanos y adyacente
1. :heavy_plus_sign: Selectores de atributos
1. :heavy_plus_sign: Selector universal
1. :heavy_plus_sign: Pseudoclases de estado
1. :heavy_plus_sign: Pseudoclases por posición y tipo
1. :heavy_plus_sign: Pseudoelementos
1. :heavy_plus_sign: Agrupación de selectores
1. :heavy_plus_sign: Cascada y especificidad
1. :heavy_plus_sign: Herencia
1. :heavy_plus_sign: Reseteo y normalización de estilos
1. :heavy_plus_sign: Prefijos de los navegadores
1. :heavy_plus_sign: Modelo de caja
1. :heavy_plus_sign: Width y height
1. :heavy_plus_sign: Border
1. :heavy_plus_sign: Margin y padding
1. :heavy_plus_sign: Cajas de línea y cajas de bloque
1. :heavy_plus_sign: Propiedad display
1. :heavy_plus_sign: Propiedad visibility
1. :heavy_plus_sign: Propiedad overflow
1. :heavy_plus_sign: Tamaño de caja
1. :heavy_plus_sign: Propiedades float y clear
1. :heavy_plus_sign: Colapso de márgenes verticales
1. :heavy_plus_sign: Adición de márgenes horizontales
1. :heavy_plus_sign: Centrado de cajas
1. :heavy_plus_sign: Posicionamiento static
1. :heavy_plus_sign: Posicionamiento relative
1. :heavy_plus_sign: Posicionamiento absolute
1. :heavy_plus_sign: Posicionamiento fixed
1. :heavy_plus_sign: Posicionamiento sticky
1. :heavy_plus_sign: Propiedad z-index
1. :heavy_plus_sign: Cabecera fixed vs cabecera sticky
1. :heavy_plus_sign: Efecto de slides con sticky
1. :heavy_plus_sign: Ventana modal menú móvil fixed
1. :heavy_plus_sign: Márgenes negativos
